const ans = function reduce(elements,cb){
    if(elements === undefined || elements === null || typeof elements === 'number'){
        return;
    }

    if(typeof cb !== 'function'){
        return;
    }

    if(!Array.isArray(elements)){
        return;
    }
    
    for(let index = 0;index<elements.length;index++){
         if(cb(elements[index],index,elements)){
            return elements[index];
         }
    }
    return undefined;
}

module.exports = ans;