const ans = function each(elements,cb){
    if(elements === undefined || elements === null || typeof elements === 'number'){
        return;
    }
    if(!Array.isArray(elements)){
        return;
    }
    if(typeof cb !== 'function'){
        return;
    }
    for(let index =0;index<elements.length;index++){
         cb(elements[index],index,elements);
    }
}
module.exports = ans;
