const ans = function reduce(elements,cb,startingValue){
    if(elements === undefined || elements === null || typeof elements === 'number'){
        return;
    }

    if(typeof cb !== 'function'){
        return;
    }

    if(!Array.isArray(elements)){
        return;
    }

    let data = startingValue;

    if(startingValue === undefined){
        data = elements[0];
    }

    for(let index = 0;index<elements.length;index++){
        if(data === elements[0] && index === 0){
            continue;
        }
         data = cb(data,elements[index],index,elements);
    }

    return data;
}

module.exports = ans;