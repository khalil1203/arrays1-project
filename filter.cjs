const ans = function reduce(elements,cb){
    if(elements === undefined || elements === null || typeof elements === 'number'){
        return [];
    }

    if(typeof cb !== 'function'){
        return [];
    }

    if(!Array.isArray(elements)){
        return [];
    }
    let data = [];

    for(let index = 0;index<elements.length;index++){
        const demo = cb(elements[index],index,elements);
         if(demo=== true){
            data.push(elements[index]);
         }
    }
    return data;
}

module.exports = ans;