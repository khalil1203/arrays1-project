const ans = function map(elements,cb){
    if(elements === undefined || elements === null || typeof elements === 'number'){
        return [];
    }
    if(typeof cb !== 'function'){
        return [];
    }
    if(!Array.isArray(elements)){
        return [];
    }
    let data =[];
    for(let index = 0;index<elements.length;index++){
        data[index] = cb(elements[index],index,elements);
    }
    return data;
}

module.exports = ans;