const ans = function flatten(elements,depth){
    if(elements === undefined || elements === null || typeof elements === 'number'){
        return [];
    }
    if(depth === undefined){
        depth=1;
    }

    let data = [];

    for(let index = 0;index<elements.length;index++){
        if (Array.isArray(elements[index])  && depth>0){
            data = data.concat(ans(elements[index],depth-1));
        }
        else if (elements[index] !== undefined){
            data.push(elements[index]);
            
        }
    }
    return data;  
}
module.exports = ans;